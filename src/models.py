from peewee import *

db = SqliteDatabase('cs305_app.db')

# Schema is created and populated with initial data as needed

def init_schema():
    db.create_tables([User])
    # TODO: insert some initial data also in it

class User(Model):
    login_id = CharField(max_length=50, unique=True)
    password_hashed = CharField(max_length=500)
    # Can be ADMIN, STAFF, PUBLIC
    role = CharField(max_length=20)
    name = CharField(max_length=50)

    class Meta:
        database = db  # This model uses the "people.db" database.
