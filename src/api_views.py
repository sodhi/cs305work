from flask import request, escape, session
from flask.templating import render_template
from passlib.handlers.pbkdf2 import pbkdf2_sha256
from models import *

def index():
    current_user = session["user"] if "user" in session else ""
    return render_template('index.html', login_id=current_user)

def login():
    try:
        login_id = request.form.get("login_id")
        password = request.form.get("password")

        user_qry = User.select().where(User.login_id == login_id)

        valid = False
        user = None
        if user_qry.exists():
            user = user_qry[0]
            valid = pbkdf2_sha256.verify(password, user.password_hashed)

        if not valid:  
            return render_template("index.html", error="Login or/and password incorrect")
        
        session['user'] = user.name
        return render_template("home.html", login_id=user.name)

    except Exception as ex:
        print("Error occurred: {}".format(ex))
        return render_template("index.html", error=str(ex))

def signup():
    try:
        if request.method == "GET":
            return render_template("register.html") 
        
        login_id = request.form.get("login_id")
        password = request.form.get("password")
        role = request.form.get("role")
        name = request.form.get("name")

        u = User(login_id=login_id, role=role, name=name)
        u.password_hashed = pbkdf2_sha256.hash(password)
        u.save()

        return render_template("index.html", error="User created successully!") 

    except Exception as ex:
        msg = "Error occurred when creating user: {}".format(ex)
        print(msg)
        return render_template("index.html", error=msg) 


def edit_user_profile():
    """It should be allowed only for the authenticated users having specific roles.

    Returns:
        [type]: [description]
    """
    # TODO: Implementation pending
    return 'Not implemented yet!'

def view_user_profile(login_id):
    if "user" in session and session["user"] == login_id:
        return "Logged in as {}".format(session["user"])
    else:
        return "Not yet implemented the view profile!"
