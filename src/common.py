from models import *

def db_connect():
    db.connect()
    print("Connected to DB")

def db_close(http_resp):
    print("Closing DB connection")
    db.close()
    return http_resp

def inject_user():
    print("Adding empty user into template context")
    return dict()