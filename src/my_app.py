from flask import Flask
from markupsafe import escape
import json
import common as C
import api_views as V

def create_app():
    the_app = Flask(__name__, static_folder="./static_files", static_url_path="/app/")
    the_app.secret_key = "eerrDDGGvh7765$$5530-987@3!"

    # Template context processor
    # the_app.context_processor(C.inject_user)
    # Hooks for request lifecycle
    the_app.before_request(C.db_connect)
    the_app.after_request(C.db_close)

    # Register views
    the_app.add_url_rule('/login', view_func=V.login, methods=['POST'])
    
    the_app.add_url_rule('/', view_func=V.index, methods=['GET'])
    
    the_app.add_url_rule('/signup', view_func=V.signup, methods=['POST', 'GET'])
    
    the_app.add_url_rule('/editprofile', view_func=V.edit_user_profile, methods=['POST'])
    
    the_app.add_url_rule('/viewprofile/<login_id>', view_func=V.view_user_profile, methods=['GET'])

    return the_app


if __name__ == "__main__":
    app = create_app()
    app.run(host="localhost", port=8999)